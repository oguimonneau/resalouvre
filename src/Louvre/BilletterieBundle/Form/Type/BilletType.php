<?php

namespace Louvre\BilletterieBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BilletType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',          TextType::class)
            ->add('prenom',       TextType::class, array(
                'label'     => 'Prénom'
            ))
            ->add('pays',         CountryType::class, array(
                'preferred_choices' => array('FR')
            ))
                
            ->add('datenaissance',BirthdayType::class, array(
                'label'  => 'Date de naissance',
                'placeholder' => array('day' => 'Jour','month' => 'Mois','year' => 'Annee')))
            ->add('reduit',CheckboxType::class, array(
                'required' => false,
                'attr' => ['class' => 'check-reduit']
                ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Louvre\BilletterieBundle\Entity\Billet'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'louvre_billetteriebundle_billet';
    }


}
