<?php

namespace Louvre\BilletterieBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


class ReservationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //On récupère la date et l'heure et on désactive l'option journée si 14 heure est passé

        $builder
            ->add('datevisite',       DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker']))
            ->add('demijournee',    ChoiceType::class,array(
                    'choices' => array(
                    'Journée' => true,
                    'Demi-journée' => false,                    
                        ),
                    ))
            ->add('email',      TextType::class)
            ->add('billets',     CollectionType::class, array(
            'entry_type'    => BilletType::class,
            'allow_add'     => true,
            'allow_delete'  =>true,
            'label'         => false    
            ))   
            ->add('Valider',SubmitType::class, array(
            'attr' => ['class' => 'btn btn-primary']    
            ));
    }
            
    
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Louvre\BilletterieBundle\Entity\Reservation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'louvre_billetteriebundle_reservation';
    }


}
