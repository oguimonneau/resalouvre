<?php

namespace Louvre\BilletterieBundle\Services;

use Doctrine\ORM\EntityRepository;
use Louvre\BilletterieBundle\Entity\Reservation;
use Symfony\Component\Templating\EngineInterface;

/**
 * Description of BilletterieService
 * 
 * @author olivier
 */
class BilletterieService {
    /**
     * @var EntityRepository
     */
    private $paramRepository;

    /**
     * @var EntityRepository
     */
    private $billetRepository;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var EngineInterface
     */
    private $templating;


    /**
     * BilletterieService constructor.
     * @param EntityRepository $paramRepository
     * @param EntityRepository $billetRepository
     * @param \Swift_Mailer $mailer
     * @param EngineInterface $templating
     */
    public function __construct(EntityRepository $paramRepository, EntityRepository $billetRepository, \Swift_Mailer $mailer, EngineInterface $templating)
    {
        $this->paramRepository = $paramRepository;
        $this->billetRepository = $billetRepository;
        $this->mailer = $mailer;
        $this->templating = $templating;
    }


    public function calculPrixBillets(Reservation $reservation) {
        foreach ($reservation->getBillets() as $billet){
            if($billet->getReduit()){
                //Tarif réduit
                $prix = $this->paramRepository->findOneBy(array('cle1' => 'prix', 'cle2' => 'reduit'))->getValeur1();
            }else{
                //Calcul du prix en fonction de l'age du visiteur
                $age = date('Y') - $billet->getDatenaissance()->format('Y');
                // On cherche le tarif en fonction de l'age
                $prix = $this->paramRepository->findTarifByAge('prix', $age)->getValeur1();
            } 
            // Tarif demi-journée ?
            if($reservation->getDemijournee()){
                $prix = $prix / 2;
            }
            $billet->setPrix($prix);
        }
    }

    public function sendEmail(Reservation $reservation)
    {
        //On récupère la réservation en session
        $message = new \Swift_Message("Musée du Louvre : Vos billets");
        $message->setFrom('contact@resalouvre.fr')
            ->setTo($reservation->getEmail())
            ->setContentType("text/html")
            ->setBody($this->templating->render('LouvreBilletterieBundle:Billet:email.html.twig', array('reservation'=> $reservation)))
        ;
        $this->mailer->send($message);      
    }

    public function calculCapacite(Reservation $reservation)
    {
        $capacite = $this->paramRepository->findOneBy(array('cle1' => 'capacite'))->getValeur1();
        $nbBilletResa = count($reservation->getBillets());
        $nbBilletsDate = $this->billetRepository->countBillets($reservation->getDatevisite());
        if ($nbBilletsDate + $nbBilletResa <= $capacite) {
            return true;
        } else {
            return false;
        }
    }
}
