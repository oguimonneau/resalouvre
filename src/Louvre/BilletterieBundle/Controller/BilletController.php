<?php

// src/Louvre/BilletterieBundle/Controller/BilletController.php

namespace Louvre\BilletterieBundle\Controller;

use Louvre\BilletterieBundle\Entity\Reservation;
use Louvre\BilletterieBundle\Form\Type\ReservationType;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;





class BilletController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        //Gestion des variables pour JQUERY
        $this->get('acme.js_vars')->jour_fermeture_num = $this->container->getParameter('jour_fermeture_num');
        $this->get('acme.js_vars')->mi_journee = $this->container->getParameter('mi_journee');
        $this->get('acme.js_vars')->fermeture_annuelle = $this->container->getParameter('fermeture_annuelle');

        //On créé un objet Réservation
        $reservation = new Reservation();
        $form = $this->createForm(ReservationType::class, $reservation);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            // On vérifie la capacité du musée sur la date choisie
            if ($this->get('louvre_billetterie.services')->calculCapacite($reservation) === false)
            {
                $this->addFlash("alert", "Plus de place disponible à cette date, veuillez choisir une autre date");
                return $this->redirectToRoute("louvre_billetterie_home");
            }

            if ($form->isValid()) {
                //On calcule le prix de chaque billet
                $this->get('louvre_billetterie.services')->calculPrixBillets($reservation);
                // On enregistre la réservation en Session
                $this->get('session')->set('resa', $reservation);
                $this->addFlash('succes', 'Votre Commande a bien été enregistrée. Pour la finaliser, vous devez maintenant proceder au payement');
                // On redirige vers la page de paiement
                return $this->redirectToRoute('louvre_billetterie_paiement');
            }
        }
        //On affiche le Template
        return $this->render('LouvreBilletterieBundle:Billet:index.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function payerAction(Request $request)
    {
        $session = $this->get('session');
        if ($session->get('resa')) {
            //On récupère la réservation session
            $reservation = $session->get('resa');

            //On affiche le Template la page de payement
            return $this->render('LouvreBilletterieBundle:Billet:payer.html.twig', array(
                'reservation' => $reservation
            ));
        } else {
            return $this->redirectToRoute("louvre_billetterie_home");
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function checkoutAction(Request $request)
    {
        $session = $this->get('session');
        if ($session->get('resa')) {
            //On récupère la réservation session
            $reservation = $session->get('resa');

            \Stripe\Stripe::setApiKey("sk_test_ASWzmBLDGmEGV28r8VxmIs6Q");
            // Get the credit card details submitted by the form
            $token = $request->request->get('stripeToken');
            // Create a charge: this will charge the user's card
            try {
                \Stripe\Charge::create(array("amount" => $reservation->getPrixReservation() * 100, "currency" => "eur", "source" => $token, "description" => "Paiement Stripe - Résalouvre"));
                //On enregistre en base de données

                $em = $this->getDoctrine()->getManager();
                $em->persist($reservation);
                $em->flush();
                return $this->redirectToRoute("louvre_billetterie_success");

            } catch (\Stripe\Error\Card $e) {
                $this->addFlash("alert", "Votre paiement n'a pas été accepté, veuillez ré-essayer");
                return $this->redirectToRoute("louvre_billetterie_paiement");
            }
        } else {
            return $this->redirectToRoute("louvre_billetterie_home");
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function successAction(Request $request)
    {
        $session = $this->get('session');
        if ($session->get('resa')->getId() !== null) {
            //La réservation a été enregistrée en base
            $reservation = $session->get('resa');
            //Envoi des billets par émail
            $this->get('louvre_billetterie.services')->sendEmail($reservation);
            $this->addFlash("succes", "Félicitations, votre paiement a bien été enregistré ! Un email contenant vos billets vous a été envoyé");
            $session->invalidate('resa');

            //On affiche le Template la page de success
            return $this->render('LouvreBilletterieBundle:Billet:success.html.twig');
        } else {
            //Cas de tentative de fraude
            $this->addFlash("alert", "Votre paiement n'a pas été accepté, veuillez ré-essayer");
            return $this->redirectToRoute("louvre_billetterie_paiement");
        }
    }
}
