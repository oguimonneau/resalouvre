<?php

namespace Louvre\BilletterieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Reservation
 *
 * @ORM\Table(name="bl_reservation")
 * @ORM\Entity(repositoryClass="Louvre\BilletterieBundle\Repository\ReservationRepository")
 * @ORM\HasLifecycleCallbacks()

 */
class Reservation
{
    /**
    * @ORM\OneToMany(targetEntity="Louvre\BilletterieBundle\Entity\Billet", mappedBy="reservation", cascade={"persist"})
    */
    
    private $billets;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=13, unique=true)
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateresa", type="datetime")
     */
    private $dateresa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datevisite", type="date")
     */
    private $datevisite;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="demijournee", type="boolean")
     */
    private $demijournee;

    /**
     * Reservation constructor.
     */
    public function __construct()
    {
        $this->dateresa = new\datetime();
        $this->payement = FALSE;
        $this->code = uniqid();
        $this->datevisite = new \Datetime();

    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Reservation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set dateresa
     *
     * @param \DateTime $dateresa
     *
     * @return Reservation
     */
    public function setDateresa($dateresa)
    {
        $this->dateresa = $dateresa;

        return $this;
    }

    /**
     * Get dateresa
     *
     * @return \DateTime
     */
    public function getDateresa()
    {
        return $this->dateresa;
    }

    /**
     * Set datevisite
     *
     * @param \DateTime $datevisite
     *
     * @return Reservation
     */
    public function setDatevisite($datevisite)
    {
        $this->datevisite = $datevisite;

        return $this;
    }

    /**
     * Get datevisite
     *
     * @return \DateTime
     */
    public function getDatevisite()
    {
        return $this->datevisite;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Reservation
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set demijournee
     *
     * @param boolean $demijournee
     *
     * @return Reservation
     */
    public function setDemijournee($demijournee)
    {
        $this->demijournee = $demijournee;

        return $this;
    }

    /**
     * Get demijournee
     *
     * @return bool
     */
    public function getDemijournee()
    {
        return $this->demijournee;
    }
    

    /**
     * Add billet
     *
     * @param \Louvre\BilletterieBundle\Entity\Billet $billet
     *
     * @return Reservation
     */
    public function addBillet(\Louvre\BilletterieBundle\Entity\Billet $billet)
    {
        $this->billets[] = $billet;
        $billet->setReservation($this);
        return $this;
    }

    /**
     * Remove billet
     *
     * @param \Louvre\BilletterieBundle\Entity\Billet $billet
     */
    public function removeBillet(\Louvre\BilletterieBundle\Entity\Billet $billet)
    {
        $this->billets->removeElement($billet);
    }

    /**
     * Get billet
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBillets()
    {
        return $this->billets;
    }
    
    public function getPrixReservation()
    {
        $prixTotal= 0;
        foreach($this->billets as $billet) {
            $prixTotal += $billet->getPrix();
        }   
        return $prixTotal;
    } 
}
