<?php

namespace Louvre\BilletterieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Param
 *
 * @ORM\Table(name="bl_param")
 * @ORM\Entity(repositoryClass="Louvre\BilletterieBundle\Repository\ParamRepository")
 */
class Param
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cle1", type="string", length=10, nullable=true)
     */
    private $cle1;

    /**
     * @var string
     *
     * @ORM\Column(name="cle2", type="string", length=10)
     */
    private $cle2;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur1", type="string", length=255)
     */
    private $valeur1;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur2", type="string", length=255)
     */
    private $valeur2;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur3", type="string", length=255)
     */
    private $valeur3;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur4", type="string", length=255)
     */
    private $valeur4;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur5", type="string", length=255)
     */
    private $valeur5;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cle1
     *
     * @param string $cle1
     *
     * @return Param
     */
    public function setCle1($cle1)
    {
        $this->cle1 = $cle1;

        return $this;
    }

    /**
     * Get cle1
     *
     * @return string
     */
    public function getCle1()
    {
        return $this->cle1;
    }

    /**
     * Set cle2
     *
     * @param string $cle2
     *
     * @return Param
     */
    public function setCle2($cle2)
    {
        $this->cle2 = $cle2;

        return $this;
    }

    /**
     * Get cle2
     *
     * @return string
     */
    public function getCle2()
    {
        return $this->cle2;
    }

    /**
     * Set valeur1
     *
     * @param string $valeur1
     *
     * @return Param
     */
    public function setValeur1($valeur1)
    {
        $this->valeur1 = $valeur1;

        return $this;
    }

    /**
     * Get valeurtxt
     *
     * @return string
     */
    public function getValeur1()
    {
        return $this->valeur1;
    }

    /**
     * Set valeur2
     *
     * @param string $valeur2
     *
     * @return Param
     */
    public function setValeur2($valeur2)
    {
        $this->valeur2 = $valeur2;

        return $this;
    }

    /**
     * Get valeur2
     *
     * @return string
     */
    public function getValeur2()
    {
        return $this->valeur2;
    }

    /**
     * Set valeur3
     *
     * @param string $valeur3
     *
     * @return Param
     */
    public function setValeur3($valeur3)
    {
        $this->valeur3 = $valeur3;

        return $this;
    }

    /**
     * Get valeur3
     *
     * @return string
     */
    public function getValeur3()
    {
        return $this->valeur3;
    }

    /**
     * Set valeur4
     *
     * @param string $valeur4
     *
     * @return Param
     */
    public function setValeur4($valeur4)
    {
        $this->valeur4 = $valeur4;

        return $this;
    }

    /**
     * Get valeur4
     *
     * @return string
     */
    public function getValeur4()
    {
        return $this->valeur4;
    }

    /**
     * Set valeur5
     *
     * @param string $valeur5
     *
     * @return Param
     */
    public function setValeur5($valeur5)
    {
        $this->valeur5 = $valeur5;

        return $this;
    }

    /**
     * Get valeur5
     *
     * @return string
     */
    public function getValeur5()
    {
        return $this->valeur5;
    }
}
