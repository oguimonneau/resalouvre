<?php

namespace Tests\Louvre\BilletterieBundle\Entity;

use Louvre\BilletterieBundle\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ReservationTest extends WebTestCase
{
    public function testSetCode()
    {
        $reservation = new Reservation();
        $this->assertEquals(13, iconv_strlen($reservation->getCode()));

    }     
}