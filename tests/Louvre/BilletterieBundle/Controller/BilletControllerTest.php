<?php

namespace Tests\Louvre\BilletterieBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BilletControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(1, $crawler->filter('h1:contains("Billetterie du Louvre")')->count());

        $form = $crawler->selectButton('louvre_billetteriebundle_reservation[Valider]')->form();
        $form['louvre_billetteriebundle_reservation[datevisite]']       = '2017-09-01';
        $form['louvre_billetteriebundle_reservation[demijournee]']      = '1';
        $form['louvre_billetteriebundle_reservation[email]']    = 'oguimonneau@gmail.com';
        $form['nb_billet']       = 1;

        $crawler = $client->submit($form);
        //$this->assertEquals(1, $crawler->filter('label:contains("Billet n°1")')->count());


    }    
    
    public function testPayer()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/payer');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
    
    public function testCheckout()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/checkout');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}
