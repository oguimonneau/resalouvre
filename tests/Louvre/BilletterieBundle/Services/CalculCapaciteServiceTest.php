<?php
namespace Louvre\BilletterieBundle\Services;

use PHPUnit\Framework\TestCase;
use Louvre\BilletterieBundle\Entity\Reservation;
use Louvre\BilletterieBundle\Entity\Param;
use Louvre\BilletterieBundle\Entity\Billet;
use Louvre\BilletterieBundle\Repository\ParamRepository;
use Louvre\BilletterieBundle\Repository\BilletRepository;
use Symfony\Bundle\TwigBundle\TwigEngine;


class BilletterieServiceTest extends TestCase
{

    /**
     * @test
     */
    public function CalculCapacite_ReturnFalse()
    {
        //On simule que la capacité max retournée sera 1000
        $param = new Param();
        $param->setValeur1(1000);
        //On crée un faux repo qui retournera le param crée précédemment (avec la valeur de 1000 qui nous interesse)
        $paramRepository = $this->createMock(ParamRepository::class);
        $paramRepository->method('findOneBy')->willReturn($param);

        //On crée un faux repo qui retournera le nombre de billets que l'on veut
        $billetRepository = $this->createMock(BilletRepository::class);
        $billetRepository->method('countBillets')->willReturn(1000);

        //On doit crée ces faux objets mais nous n'en aurons pas besoin
        $mailer = $this->createMock(\Swift_Mailer::class);
        $templating = $this->createMock(TwigEngine::class);

        //On crée enfin le service avec tous nos faux objets
        $service = new BilletterieService($paramRepository, $billetRepository, $mailer, $templating);


        //reservation que l'on va utiliser pour le test
        $reservation = new Reservation();
        //on ajoute un billet à la reservation
        $billet = new Billet();
        $reservation->addBillet($billet);
        

        //On teste la valeur renvoyée par le service (ce qui nous interesse)
        //La capacité est de 1000, et il y a 1000 billets + 1 ca renvoie donc false
        $this->assertFalse($service->calculCapacite($reservation));
    }
    /**
     * @test
     */
    public function CalculCapacite_ReturnTrue()
    {
        //On simule que la capacité max retournée sera 1000
        $param = new Param();
        $param->setValeur1(1000);

        //On crée un faux repo qui retournera le param crée précédemment (avec la valeur de 1000 qui nous interesse)
        $paramRepository = $this->createMock(ParamRepository::class);
        $paramRepository->method('findOneBy')->willReturn($param);

        //On crée un faux repo qui retournera le nombre de billets que l'on veut
        $billetRepository = $this->createMock(BilletRepository::class);
        $billetRepository->method('countBillets')->willReturn(500);

        //On doit crée ces faux objets mais nous n'en aurons pas besoin
        $mailer = $this->createMock(\Swift_Mailer::class);
        $templating = $this->createMock(TwigEngine::class);

        //On crée enfin le service avec tous nos faux objets
        $service = new BilletterieService($paramRepository, $billetRepository, $mailer, $templating);

        //reservation que l'on va utiliser pour le test
        $reservation = new Reservation();
        //on ajoute un billet à la reservation
        $billet = new Billet();
        $reservation->addBillet($billet);


        //On teste la valeur renvoyée par le service (ce qui nous interesse)
        //La capacité est de 1000, et il y a 500 billets + 1 ca renvoie donc true
        $this->assertTrue($service->calculCapacite($reservation));
    }
}