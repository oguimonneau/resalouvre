$(function() {
    var heureMax = 18;
    // Récupération des variables définies dans le service js_vars
    var JsVars = jQuery('#js-vars').data('vars');
    var jour_fermeture_num = JsVars.jour_fermeture_num;
    var mi_journee = JsVars.mi_journee;
    var fermeture_annuelle = JsVars.fermeture_annuelle;
    
    // On récupère la balise <div> en question qui contient l'attribut « data-prototype » qui nous intéresse.
    var $container = $('div#louvre_billetteriebundle_reservation_billets');    
    // On définit un compteur unique pour nommer les champs qu'on va ajouter dynamiquement
    var index = $container.find(':input').length;

    // On ajoute autant de champ que saisi.
    $('#nb_billet').blur(function(e) {
        var nb_billet = e.target.value;
        var i;
        //On supprime tous les champs pottentiellement en place        
        for (i = 1; i <= index; i++){
            $('.detail').remove();
        }
        i = 0;
        index = 0;
        for (i = 1; i <= nb_billet; i++){
            addBillet($container);
        }
        chargeReduit();
        e.preventDefault(); // évite qu'un # apparaisse dans l'URL
        return false;
    });

    // La fonction qui ajoute un formulaire CategoryType
    function addBillet($container) {
      // Dans le contenu de l'attribut « data-prototype », on remplace :
      // - le texte "__name__label__" qu'il contient par le label du champ
      // - le texte "__name__" qu'il contient par le numéro du champ
      var template = $container.attr('data-prototype')
        .replace(/__name__label__/g, 'Billet n°' + (index+1))
        .replace(/__name__/g,        index)
        .replace(/form-group/g,'form-group detail')
      ;
      // On crée un objet jquery qui contient ce template
      var $prototype = $(template);

      // On ajoute au prototype un lien pour pouvoir supprimer la catégorie
      addDeleteLink($prototype);

      // On ajoute le prototype modifié à la fin de la balise <div>
      $container.append($prototype);

      // Enfin, on incrémente le compteur pour que le prochain ajout se fasse avec un autre numéro
      index++;
    }

    // La fonction qui ajoute un lien de suppression d'une catégorie
    function addDeleteLink($prototype) {
      // Création du lien
      var $deleteLink = $('<a href="#" class="btn btn-danger">Supprimer</a></br>');

      // Ajout du lien
      $prototype.append($deleteLink);

      // Ajout du listener sur le clic du lien pour effectivement supprimer la catégorie
      $deleteLink.click(function(e) {
        $prototype.remove();
        e.preventDefault(); // évite qu'un # apparaisse dans l'URL
        return false;
      });
    }
    $('.js-datepicker').change(function(e) {
        var date = new Date();
        // Si jour = Mardi ou Heure > 18 alors on passe au lendemain
        console.log(date.getDay());
        if (date.getHours() > heureMax || date.getDay() == 2){
            date.setDate(date.getDate()+1);
        }
        if(date.getMonth()+1 < 10){
            var mois = '0'+(date.getMonth()+1);
        }else{
            var mois = date.getMonth()+1;
        }
        if(date.getDate() < 10){
            var jour = '0'+date.getDate();
        }else{
            var jour = date.getDate();
        }
        var heure = date.getHours();
        var now = date.getFullYear()+'-'+mois+'-'+jour;
        
        if(heure > heureMax){
            e.target.value = now;
            //On initialise l'heure au matin
            date.setHours(8);
        }
        
        document.getElementById("louvre_billetteriebundle_reservation_demijournee").innerHTML = "";
        if(now == e.target.value && date.getHours() > mi_journee ){
            document.getElementById("louvre_billetteriebundle_reservation_demijournee").innerHTML += '<option value="1">Demi-journée (à partir de '+mi_journee+' heure )</option>';
        }else{
            document.getElementById("louvre_billetteriebundle_reservation_demijournee").innerHTML += '<option value="0">Journée</option>';
            document.getElementById("louvre_billetteriebundle_reservation_demijournee").innerHTML += '<option value="1">Demi-journée (à partir de '+mi_journee+' heure )</option>';
        }
    });
    var date = new Date();
    if (date.getHours()> heureMax ){
        $('.js-datepicker').datepicker({
            language: "fr",
            format: "yyyy-mm-dd",
            daysOfWeekDisabled: '"'+jour_fermeture_num+'"',
            todayHighlight: true,
            startDate:"+1d",
            todayBtn: true,
            autoclose: true,
            datesDisabled: fermeture_annuelle
        });
    }else{
        $('.js-datepicker').datepicker({
            language: "fr",
            format: "yyyy-mm-dd",
            daysOfWeekDisabled: '"'+jour_fermeture_num+'"',
            todayHighlight: true,
            startDate:"-d",
            todayBtn: true,
            autoclose: true,
            datesDisabled: fermeture_annuelle
        });        
    }
    function chargeReduit(){
        $('.check-reduit').change(function(e) {
            if (this.checked) {
                $(this).before('<p class = "check_text alert alert-danger">Vous devrez présenter vos pièces justificatives au guichet</p>');
            }else{
                $('.check_text').remove()
            }
        });
    }
});


